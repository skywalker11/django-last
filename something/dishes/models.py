from django.db import models
from django.utils import timezone

# Create your models here.

class Chef(models.Model):
    fio = models.CharField(
        verbose_name  = 'ФИО',
        max_length = 60,
        null = False,
        blank = False
         )
    
    age = models.IntegerField(
        verbose_name = 'Возраст',
        null = False,
        blank = False
        )
    
    phone = models.IntegerField(
        verbose_name = 'Номер телефона',
        null = False,
        blank = False
        )
    
    def __str__(self):
        return self.fio
    
class Dish(models.Model):
    title = models.CharField(
        verbose_name = 'Название блюда',
        max_length = 45,
        null = False,
        blank = False
        )
    
    chef = models.ForeignKey(
        'Chef', 
        on_delete = models.CASCADE
        )
    
    price = models.IntegerField(
        verbose_name = 'Стоимость',
        null = False,
        blank = False
        )
    
    def __str__(self):
        return self.title  
    
class Ingredient(models.Model):
    title = models.CharField(
        verbose_name = 'Название ингридиента',
        max_length = 40,
        null = False,
        blank = False
        )
    
    dish = models.ForeignKey(
        'Dish', 
        on_delete = models.CASCADE
    )
    
    provider = models.CharField(
        verbose_name = 'Поставщик/Изготовитель',
        max_length = 50,
        )
    
    def __str__(self):
        return self.title
    

    
    
    
