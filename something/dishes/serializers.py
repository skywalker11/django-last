from rest_framework import serializers
from .models import Dish, Chef, Ingredient


class DishSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dish
        fields = ('id','title', 'chef', 'price')

class ChefSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chef
        fields = ('id','fio', 'age', 'phone')
        
class IngredientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ingredient
        fields = ('id','title', 'dish', 'provider')